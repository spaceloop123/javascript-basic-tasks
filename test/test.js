const assert = require('assert');

const {
    deafPhoneGame,
    nameHockeyGameWinner,
    swapTwoElementsInArray,
    isBracketsBalanced,
    memoize,
} = require('../src');

describe('deafPhoneGame', () => {
    it('Should return "hi" for given input "hi"', () => {
        const value = deafPhoneGame('hi');

        assert.equal(value, 'hi');
    });

    it('Should return 123 for given input 123', () => {
        const value = deafPhoneGame(123);

        assert.equal(value, 123);
    });

    it('Should return {name: "Name"} for given input {name: "Name"}', () => {
        const value = deafPhoneGame({ name: 'Name' });

        assert.deepEqual(value, { name: 'Name' });
    });
});

describe('nameHockeyGameWinner', () => {
    it('Should return "Bolts defeat Capitals with the score 5:4" for given input', () => {
        const value = nameHockeyGameWinner('Bolts', '5:4', 'Capitals');

        assert.equal(value, 'Bolts defeat Capitals with the score 5:4');
    });

    it('Should return "Bruins defeat Red Wings with the score 4:2" for given input', () => {
        const value = nameHockeyGameWinner('Red Wings', '2:4', 'Bruins');

        assert.equal(value, 'Bruins defeat Red Wings with the score 4:2');
    });

    it('Should return "Flames defeat Stars in OT with the score 7:6OT" for given input', () => {
        const value = nameHockeyGameWinner('Stars', '6:7OT', 'Flames');

        assert.equal(value, 'Flames defeat Stars in OT with the score 7:6OT');
    });
});

describe('swapTwoElementsInArray', () => {
    it('Should return "[3, 12, 65]" for given input', () => {
        const value = swapTwoElementsInArray([65, 12, 3], 0, 2);

        assert.equal(value, [3, 12, 65]);
    });

    it('Should return "[3, 4, -1, 12, 65]" for given input', () => {
        const value = swapTwoElementsInArray([3, 4, 65, 12, -1], 2, 4);

        assert.equal(value, [3, 4, -1, 12, 65]);
    });

    it('Should return "[3, 4, 65, 12, -1]" for given input', () => {
        const value = swapTwoElementsInArray([3, 4, 65, 12, -1], -100, 4);

        assert.equal(value, [3, 4, 65, 12, -1]);
    });
});

describe('isBracketsBalanced', () => {
    it('Should return true for given input', () => {
        const value = isBracketsBalanced('[[][]]');

        assert.equal(value, true);
    });

    it('Should return true for given input', () => {
        const value = isBracketsBalanced('[{}]');

        assert.equal(value, true);
    });

    it('Should return false for given input', () => {
        const value = isBracketsBalanced('({}[]<>(((())))');

        assert.equal(value, false);
    });
});

describe('memoize', () => {
    let numberOfCalls = 0;
    const fn = function() {
        numberOfCalls++;
        return Math.random();
    };

    const memoizer = memoize(fn);
    let expected = memoizer();

    assert.equal(numberOfCalls, 1, 'memoize result should evaluate the specified function at first call');

    for(let i = 0; i < 10; i++) {
        let actual = memoizer();

        assert.equal(actual, expected, 'memoize result should return the cached value at second and next calls');
        assert.equal(numberOfCalls, 1, 'memoize result should not evaluate the specified function at second and next calls');
    }
});
